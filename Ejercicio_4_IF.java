package ejerciciosIfyCiclos;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import javax.swing.Action;

public class Ejercicio_4_IF {

	private JFrame frame;
	private JTextField textField;


	/**
	 * Launch the application.
	 */
	int seleccion;
	private final Action action = new SwingAction();
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio_4_IF window = new Ejercicio_4_IF();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio_4_IF() {
		initialize();
	}

	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingrese y seleccione el nombre para ver su categoria:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 39, 434, 17);
		frame.getContentPane().add(lblNewLabel);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int seleccion;
				seleccion= comboBox.getSelectedIndex();
				comboBox.getSelectedIndex();
				if (seleccion == 1){
 					textField.setText(String.valueOf("HIJO"));
					}
				if (seleccion == 2){
 					textField.setText(String.valueOf("PADRE"));
					}
				if (seleccion == 3){
	 				textField.setText(String.valueOf("ABUELO"));
						}
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"", "Boca Juniors", "San Lorenzo", "River Plate"}));
		comboBox.setBounds(149, 85, 150, 17);
		frame.getContentPane().add(comboBox);
		
		JLabel lblCategoria = new JLabel("Categoria:");
		lblCategoria.setHorizontalAlignment(SwingConstants.CENTER);
		lblCategoria.setBounds(0, 127, 434, 17);
		frame.getContentPane().add(lblCategoria);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(149, 155, 150, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textField.setText(null);
				
			}
		});
		btnAceptar.setAction(action);
		btnAceptar.setBounds(169, 212, 89, 23);
		frame.getContentPane().add(btnAceptar);
	}
	@SuppressWarnings("serial")
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Aceptar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
