package ejerciciosIfyCiclos;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;


import java.awt.event.ActionListener;
import java.awt.SystemColor;

public class Ejercicio_6_IF {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private final Action action = new SwingAction();

	/**
	 * Launch the application.
	 */
	String CONDICION, a, a1;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio_6_IF window = new Ejercicio_6_IF();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio_6_IF() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.menu);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblIngresarNombreO = new JLabel("Ingresa el numero del curso que desea saber donde se encuentra:");
		lblIngresarNombreO.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngresarNombreO.setBounds(0, 0, 434, 20);
		frame.getContentPane().add(lblIngresarNombreO);
		
		JLabel lblNewLabel = new JLabel("Curso:");
		lblNewLabel.setBounds(71, 54, 86, 14);
		frame.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(197, 51, 131, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblSuCursoEsta = new JLabel("Su curso esta en:");
		lblSuCursoEsta.setBounds(47, 129, 115, 14);
		frame.getContentPane().add(lblSuCursoEsta);
		
		textField_1 = new JTextField();
		textField_1.setBackground(Color.WHITE);
		textField_1.setEditable(false);
		textField_1.setBounds(197, 123, 131, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnAverigue = new JButton("Averigue");
		btnAverigue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int a1;
				a=(textField.getText());
				a1=Integer.parseInt(textField.getText());
				if(a1==0) {
					CONDICION = "Jardin de infantes";
					textField_1.setForeground(Color.CYAN);
					textField_1.setText(String.valueOf(CONDICION));
				}
				if(a1>0 && a1<=7 ) {
					CONDICION = "Primaria";
					textField_1.setForeground(Color.DARK_GRAY);
					textField_1.setText(String.valueOf(CONDICION));
				}
				if(a1>=7 && a1<=12 ) {
					CONDICION = "Secundaria";
					textField_1.setForeground(Color.BLACK);
					textField_1.setText(String.valueOf(CONDICION));
				}
				if(a1>12) {
				 JOptionPane.showMessageDialog(null, "Error,Digite un a�o entre el rango de 0-12");
				}
			}
		});
		btnAverigue.setAction(action);
		btnAverigue.setBounds(167, 209, 89, 23);
		frame.getContentPane().add(btnAverigue);
	}

	@SuppressWarnings("serial")
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Averigue");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}