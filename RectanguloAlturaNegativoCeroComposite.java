package objetos.controller.composite;

import objetos.modelo.Rectangulo;

public class RectanguloAlturaNegativoCeroComposite extends ValidatorComposite {

	public RectanguloAlturaNegativoCeroComposite() {
	
	}

	@Override
	public boolean isMe() {
		return figura instanceof Rectangulo;
	}

	@Override
	public boolean validar() {
		Rectangulo rec = (Rectangulo)figura;
		return rec.getAltura()<=0;
	
	}

	@Override
	public String getError() {
		// hacer un downcast
		return "La altura debe ser mayor que 0 (cero)";
	}

}

